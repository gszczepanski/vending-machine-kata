package szczepanski.gerard.vendingmacinekata.domain.vendingmachine

import org.assertj.core.api.Assertions.assertThat
import org.scalatest.FunSpec

class IdSpec extends FunSpec {

  describe("An Id") {

    it("should generate Id object") {
      val id = Id.unique()

      assertThat(id) isNotNull
    }

    it("should generate unique Id object") {
      val first = Id.unique()
      val second = Id.unique()

      assertThat(first) isNotEqualTo second
    }

  }

}
