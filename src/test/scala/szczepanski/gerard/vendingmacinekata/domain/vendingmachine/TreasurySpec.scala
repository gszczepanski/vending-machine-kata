package szczepanski.gerard.vendingmacinekata.domain.vendingmachine

import org.assertj.core.api.Assertions.assertThat
import org.scalatest.FunSpec

class TreasurySpec extends FunSpec with ProductsFixture {

  describe("A TreasurySpec") {

    it("should add Denomination to temporary money on put action") {
      val treasury = new Treasury()

      treasury.put(FiveZl())

      assertThat(treasury.revertTemporaryMoney().denominationQuantityMap(FiveZl())) isEqualTo 1
    }

    it("should calculate remaining required value for Product price") {
      val treasury = new Treasury()

      treasury.put(OneZl())
      val remainingMoney = treasury.howMuchMoneyIsStillRequiredFor(cocaCola())

      assertThat(remainingMoney) isEqualTo BigDecimal(1.99)
    }

    it("should return TRUE when inserted money is enough for Product") {
      val treasury = new Treasury()

      treasury.put(FiveZl())

      assertThat(treasury.isInsertedMoneyEnoughFor(cocaCola())) isTrue
    }

    it("should return FALSE when inserted money is not enough for Product") {
      val treasury = new Treasury()

      treasury.put(OneZl())

      assertThat(treasury.isInsertedMoneyEnoughFor(cocaCola())) isFalse
    }

    it("should return TRUE when inserted money is greater than Product price and we need to return change") {
      val treasury = new Treasury()

      treasury.put(FiveZl())

      assertThat(treasury.isChangeRequiredFor(cocaCola())) isTrue
    }

    it("should return FALSE when inserted money is equal Product price and we do not need to return change") {
      val treasury = new Treasury()

      treasury.put(OneZl())
      treasury.put(FiftyGr())

      assertThat(treasury.isChangeRequiredFor(snickers())) isFalse
    }

    it("Should transfer money from temporaryMoney to machineMoney on transferTemporaryMoneyToMachineMoney") {
      val treasury = new Treasury()

      treasury.put(FiveZl())
      treasury.transferTemporaryMoneyToMachineMoney()

      assertThat(treasury.revertTemporaryMoney().denominationQuantityMap) isEqualTo Map(
        FiveZl() -> 0,
        TwoZl() -> 0,
        OneZl() -> 0,
        FiftyGr() -> 0,
        TwentyGr() -> 0,
        TenGr() -> 0
      )
    }

    it("Should return TRUE when we ask whether it can return change or not when machine money is enough to return change") {
      val treasury = new Treasury()

      treasury.put(OneZl())
      treasury.put(OneZl())
      treasury.put(FiftyGr())
      treasury.transferTemporaryMoneyToMachineMoney()

      treasury.put(TwoZl())

      assertThat(treasury.canReturnChangeFor(snickers())) isTrue
    }

    it("Should return FALSE when we ask whether it can return change or not when machine money is not enough to return change") {
      val treasury = new Treasury()

      treasury.put(OneZl())
      treasury.put(OneZl())
      treasury.transferTemporaryMoneyToMachineMoney()

      treasury.put(TwoZl())

      assertThat(treasury.canReturnChangeFor(snickers())) isFalse
    }

  }

}
