package szczepanski.gerard.vendingmacinekata.domain.vendingmachine

import org.assertj.core.api.Assertions.assertThat
import org.scalatest.FunSpec


class ProductSpec extends FunSpec {

  describe("A Product") {

    it("should be created with unique id") {
      val cocaCola = Product("Coca-cola 0.33l", Price(2.49))

      assertThat(cocaCola.id) isNotNull
    }
  }

}
