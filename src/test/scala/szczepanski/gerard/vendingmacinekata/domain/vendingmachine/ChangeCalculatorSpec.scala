package szczepanski.gerard.vendingmacinekata.domain.vendingmachine

import org.assertj.core.api.Assertions.assertThat
import org.scalatest.FunSpec

class ChangeCalculatorSpec extends FunSpec {

  describe("A ChangeCalculator should return true for change for case") {

    it("Should return true for change 9 zl and values 1:2:0:0:0:0") {
      val bag = new MoneyBag()
      bag.addMany(Map(
        FiveZl() -> 1,
        TwoZl() -> 2
      ))

      val result = ChangeCalculator.fetchChangeFor(9.00, bag)

      assertThat(result.canReturnChange) isTrue()
      assertThat(result.requiredDenominations) isEqualTo Map(
        FiveZl() -> 1,
        TwoZl() -> 2
      )
    }

    it("Should return true for change 9 zl and values 0:5:2:0:0:0") {
      val bag = new MoneyBag()
      bag.addMany(Map(
        TwoZl() -> 5,
        OneZl() -> 2
      ))

      val result = ChangeCalculator.fetchChangeFor(9.00, bag)

      assertThat(result.canReturnChange) isTrue()
      assertThat(result.requiredDenominations) isEqualTo Map(
        TwoZl() -> 4,
        OneZl() -> 1
      )
    }

    it("Should return true for change 9 zl and values 1:0:3:3:0:0") {
      val bag = new MoneyBag()
      bag.addMany(Map(
        FiveZl() -> 1,
        OneZl() -> 3,
        FiftyGr() -> 3
      ))

      val result = ChangeCalculator.fetchChangeFor(9.00, bag)

      assertThat(result.canReturnChange) isTrue()
      assertThat(result.requiredDenominations) isEqualTo Map(
        FiveZl() -> 1,
        OneZl() -> 3,
        FiftyGr() -> 2
      )
    }

    it("Should return true for change 9 zl and values 1:0:3:0:5:0") {
      val bag = new MoneyBag()
      bag.addMany(Map(
        FiveZl() -> 1,
        OneZl() -> 3,
        TwentyGr() -> 5
      ))

      val result = ChangeCalculator.fetchChangeFor(9.00, bag)

      assertThat(result.canReturnChange) isTrue()
      assertThat(result.requiredDenominations) isEqualTo Map(
        FiveZl() -> 1,
        OneZl() -> 3,
        TwentyGr() -> 5
      )
    }

    it("Should return true for change 9 zl and values 1:0:3:0:4:3") {
      val bag = new MoneyBag()
      bag.addMany(Map(
        FiveZl() -> 1,
        OneZl() -> 3,
        TwentyGr() -> 4,
        TenGr() -> 3
      ))

      val result = ChangeCalculator.fetchChangeFor(9.00, bag)

      assertThat(result.canReturnChange) isTrue()
      assertThat(result.requiredDenominations) isEqualTo Map(
        FiveZl() -> 1,
        OneZl() -> 3,
        TwentyGr() -> 4,
        TenGr() -> 2
      )
    }

    it("Should return true for change 6.50 zl and values 1:1:1:3:0:0") {
      val bag = new MoneyBag()
      bag.addMany(Map(
        FiveZl() -> 1,
        TwoZl() -> 1,
        OneZl() -> 1,
        FiftyGr() -> 3
      ))

      val result = ChangeCalculator.fetchChangeFor(6.50, bag)

      assertThat(result.canReturnChange) isTrue()
      assertThat(result.requiredDenominations) isEqualTo Map(
        FiveZl() -> 1,
        OneZl() -> 1,
        FiftyGr() -> 1
      )
    }

    it("Should return true for change 6.50 zl and values 0:4:1:0:4:4") {
      val bag = new MoneyBag()
      bag.addMany(Map(
        TwoZl() -> 4,
        OneZl() -> 1,
        TwentyGr() -> 4,
        TenGr() -> 4
      ))

      val result = ChangeCalculator.fetchChangeFor(6.50, bag)

      assertThat(result.canReturnChange) isTrue()
      assertThat(result.requiredDenominations) isEqualTo Map(
        TwoZl() -> 3,
        TwentyGr() -> 2,
        TenGr() -> 1
      )
    }
  }

  describe("A ChangeCalculator should return false for change for case") {

    it("Should return false for change 9 zl and values 0:2:0:0:0:0") {
      val bag = new MoneyBag()
      bag.addMany(Map(
        TwoZl() -> 2
      ))

      val result = ChangeCalculator.fetchChangeFor(9.00, bag)

      assertThat(result.canReturnChange) isFalse()
      assertThat(result.requiredDenominations) isEqualTo Map()
    }

  it("Should return false for change 9 zl and values 5:1:1:0:0:0") {
      val bag = new MoneyBag()
      bag.addMany(Map(
        FiveZl() -> 5,
        TwoZl() -> 1,
        OneZl() -> 1
      ))

      val result = ChangeCalculator.fetchChangeFor(9.00, bag)

      assertThat(result.canReturnChange) isFalse()
      assertThat(result.requiredDenominations) isEqualTo Map()
    }
  }

}
