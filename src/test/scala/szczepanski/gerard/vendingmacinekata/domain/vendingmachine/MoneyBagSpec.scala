package szczepanski.gerard.vendingmacinekata.domain.vendingmachine

import org.assertj.core.api.Assertions.assertThat
import org.scalatest.FunSpec

class MoneyBagSpec extends FunSpec {

  val FIVE_ZL = FiveZl()

  describe("A MoneyBag") {

    it("create with all denomination quantity equals ZERO") {
      val moneyBag = new MoneyBag()

      assertThat(moneyBag.howMany(FiveZl())) isEqualTo 0
      assertThat(moneyBag.howMany(TwoZl())) isEqualTo 0
      assertThat(moneyBag.howMany(OneZl())) isEqualTo 0
      assertThat(moneyBag.howMany(FiftyGr())) isEqualTo 0
      assertThat(moneyBag.howMany(TwentyGr())) isEqualTo 0
      assertThat(moneyBag.howMany(TenGr())) isEqualTo 0
    }

    it("increase Denomination on add") {
      val moneyBag = new MoneyBag()

      moneyBag.add(FIVE_ZL)

      assertThat(moneyBag.howMany(FIVE_ZL)) isEqualTo 1
    }

    it("throw IllegalStateExeption when Denomination quantity is ZERO during subtract") {
      val moneyBag = new MoneyBag()

      assertThrows[IllegalStateException] {
        moneyBag.subtract(FIVE_ZL, 100)
      }
    }
  }

  it("throw IllegalArgumentExeption when quantity of subtract is greater than Denomination quantity in bag") {
    val moneyBag = new MoneyBag()
    moneyBag.add(FIVE_ZL)

    assertThrows[IllegalArgumentException] {
      moneyBag.subtract(FIVE_ZL, 2)
    }
  }

  it("subtract given quantity of Denomination") {
    val moneyBag = new MoneyBag()
    moneyBag.add(FIVE_ZL)
    moneyBag.add(FIVE_ZL)

    moneyBag.subtract(FIVE_ZL, 2)

    assertThat(moneyBag.howMany(FIVE_ZL)) isEqualTo 0
  }

  it("empty money map on withdraw action") {
    val moneyBag = new MoneyBag()
    moneyBag.add(FIVE_ZL)

    moneyBag.withdraw()

    assertThat(moneyBag.howMany(FIVE_ZL)) isEqualTo 0
  }

  it("return all money as Map on withdraw") {
    val moneyBag = new MoneyBag()
    moneyBag.add(FIVE_ZL)

    val withdrawedMoney = moneyBag.withdraw()

    assertThat(withdrawedMoney(FiveZl())) isEqualTo 1
    assertThat(withdrawedMoney(TwoZl())) isEqualTo 0
    assertThat(withdrawedMoney(OneZl())) isEqualTo 0
    assertThat(withdrawedMoney(FiftyGr())) isEqualTo 0
    assertThat(withdrawedMoney(TwentyGr())) isEqualTo 0
    assertThat(withdrawedMoney(TenGr())) isEqualTo 0
  }

  it("should sum denominations in bag on total call") {
    val moneyBag = new MoneyBag()

    moneyBag.add(OneZl())
    moneyBag.add(TwoZl())
    moneyBag.add(FiveZl())

    assertThat(moneyBag.total()) isEqualTo BigDecimal(8.00)
  }

  it("should add many denominations to bag") {
    val moneyBag = new MoneyBag()

    moneyBag.addMany(Map(
      FiveZl() -> 1,
      TwoZl() -> 2
    ))

    assertThat(moneyBag.howMany(FiveZl())) isEqualTo 1
    assertThat(moneyBag.howMany(TwoZl())) isEqualTo 2
    assertThat(moneyBag.howMany(OneZl())) isEqualTo 0
    assertThat(moneyBag.howMany(FiftyGr())) isEqualTo 0
    assertThat(moneyBag.howMany(TwentyGr())) isEqualTo 0
    assertThat(moneyBag.howMany(TenGr())) isEqualTo 0
  }

}
