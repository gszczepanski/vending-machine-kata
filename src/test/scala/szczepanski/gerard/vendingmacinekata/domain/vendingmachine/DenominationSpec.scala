package szczepanski.gerard.vendingmacinekata.domain.vendingmachine

import org.assertj.core.api.Assertions.assertThat
import org.scalatest.FunSpec


class DenominationSpec extends FunSpec {

  describe("A Denomination of") {

    it("FiveZl should return 5.0 value") {
      val denomination = FiveZl()
      assertThat(denomination.value().bigDecimal.doubleValue()) isEqualTo 5.0
    }

    it("TwoZl should return 2.0 value") {
      val denomination = TwoZl()
      assertThat(denomination.value().bigDecimal.doubleValue()) isEqualTo 2.0
    }

    it("OneZl should return 1.0 value") {
      val denomination = OneZl()
      assertThat(denomination.value().bigDecimal.doubleValue()) isEqualTo 1.0
    }

    it("FiftyGr should return 0.5 value") {
      val denomination = FiftyGr()
      assertThat(denomination.value().bigDecimal.doubleValue()) isEqualTo 0.5
    }

    it("TwentyGr should return 0.2 value") {
      val denomination = TwentyGr()
      assertThat(denomination.value().bigDecimal.doubleValue()) isEqualTo 0.2
    }

    it("FiftyGr should return 0.1 value") {
      val denomination = TenGr()
      assertThat(denomination.value().bigDecimal.doubleValue()) isEqualTo 0.1
    }

  }

}
