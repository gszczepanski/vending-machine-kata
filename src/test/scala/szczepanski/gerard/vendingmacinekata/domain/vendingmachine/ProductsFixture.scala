package szczepanski.gerard.vendingmacinekata.domain.vendingmachine

trait ProductsFixture {

  def cocaCola() : Product = Product("Coca cola 0.33 l", Price(2.99))

  def snickers() : Product = Product("Snickers", Price(1.50))

  def aloeVeraKing() : Product = Product("Aloe Vera King 0.5 l", Price(6.39))

}
