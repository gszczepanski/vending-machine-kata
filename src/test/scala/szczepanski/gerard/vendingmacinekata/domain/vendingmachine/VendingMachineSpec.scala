package szczepanski.gerard.vendingmacinekata.domain.vendingmachine

import org.assertj.core.api.Assertions.assertThat

import org.scalamock.scalatest.MockFactory
import org.scalatest.FunSpec


class VendingMachineSpec extends FunSpec with MockFactory with ShelvesFixture {

  private val TWO_ZL = TwoZl()

  describe("A VendingMachine") {

    it("should pass Product from selected Shelve on selectProduct method") {
      val displayStub = stub[Display]
      val vendingMachine = new VendingMachine(shelves(), displayStub, new Treasury())

      vendingMachine.selectProduct(1)

      (displayStub.displayProductPrice _).verify(cocaCola())
    }

    it("should throw IllegalStateException when Product is not selected and we insert money") {
      val vendingMachine = new VendingMachine(shelves(), new Display(), new Treasury())

      assertThrows[IllegalStateException] {
        vendingMachine.insertCoin(TWO_ZL)
      }
    }

    it("should add Denomination to Treasury on insertCoin") {
      val treasury = new Treasury()
      val vendingMachine = new VendingMachine(shelves(), new Display(), treasury)

      vendingMachine.selectProduct(1)
      vendingMachine.insertCoin(TWO_ZL)

      val revertedInsertedMoney = treasury.revertTemporaryMoney().denominationQuantityMap
      assertThat(revertedInsertedMoney(TWO_ZL)) isEqualTo 1
    }

    it("should return inserted money on cancel") {
      val vendingMachine = new VendingMachine(shelves(), new Display(), new Treasury())

      vendingMachine.selectProduct(1)
      vendingMachine.insertCoin(TWO_ZL)

      val insertedMoney = vendingMachine.cancel()
      assertThat(insertedMoney.denominationQuantityMap(TWO_ZL)) isEqualTo 1
    }


    it("should clear selected product on cancel so if we want to insert coin after cancel we got exception") {
      val vendingMachine = new VendingMachine(shelves(), new Display(), new Treasury())

      vendingMachine.selectProduct(1)
      vendingMachine.cancel()

      assertThrows[IllegalStateException] {
        vendingMachine.insertCoin(TWO_ZL)
      }
    }

    it("should display remaining money value for Product") {
      val displayStub = stub[Display]
      val vendingMachine = new VendingMachine(shelves(), displayStub, new Treasury())

      vendingMachine.selectProduct(1)
      vendingMachine.insertCoin(TwoZl())

      (displayStub.displayRemainingValueForProduct _).verify(BigDecimal(0.99))
    }

    it("should return Product and no change when change is not required") {
      val vendingMachine = new VendingMachine(shelves(), new Display(), new Treasury())

      vendingMachine.selectProduct(3)
      vendingMachine.insertCoin(OneZl())
      vendingMachine.insertCoin(TwentyGr())
      vendingMachine.insertCoin(TwentyGr())
      val insertCoinResult = vendingMachine.insertCoin(TenGr())

      assertThat(insertCoinResult.product.get) isEqualTo snickers()
      assertThat(insertCoinResult.money.isEmpty) isTrue
    }

    it("Should print warning when change can not be retuned") {
      val displayStub = stub[Display]
      val vendingMachine = new VendingMachine(shelves(), displayStub, new Treasury())

      vendingMachine.selectProduct(3)
      vendingMachine.insertCoin(TwoZl())

      (displayStub.displayWarningThatChangeCanNotBeReturned _).verify()
    }

    it("Should return inserted coins when Machine can not return change") {
      val vendingMachine = new VendingMachine(shelves(), new Display(), new Treasury())

      vendingMachine.selectProduct(3)
      val result = vendingMachine.insertCoin(TwoZl())

      assertThat(result.money.get.denominationQuantityMap) isEqualTo Map(
        FiveZl() -> 0,
        TwoZl() -> 1,
        OneZl() -> 0,
        FiftyGr() -> 0,
        TwentyGr() -> 0,
        TenGr() -> 0
      )
    }

    it("Should not return Product when Machine can not return change") {
      val vendingMachine = new VendingMachine(shelves(), new Display(), new Treasury())

      vendingMachine.selectProduct(3)
      val result = vendingMachine.insertCoin(TwoZl())

      assertThat(result.product.isEmpty) isTrue
    }
  }
}
