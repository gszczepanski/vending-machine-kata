package szczepanski.gerard.vendingmacinekata.domain.vendingmachine

trait ShelvesFixture extends ProductsFixture {

  def shelves(): Map[Int, Shelve] = Map[Int, Shelve](
    1 -> new Shelve(1, cocaCola(), 5),
    2 -> new Shelve(2, aloeVeraKing(), 5),
    3 -> new Shelve(3, snickers(), 5)
  )

}
