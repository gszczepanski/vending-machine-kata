package szczepanski.gerard.vendingmacinekata.domain.vendingmachine

import org.assertj.core.api.Assertions.assertThat
import org.scalatest.FunSpec

class ShelveSpec extends FunSpec with ProductsFixture {

  describe("A Shelve") {

    it("should create object for given params") {
      val product = cocaCola()
      val shelve = new Shelve(1, product, 5)

      assertThat(shelve.number) isEqualTo 1
      assertThat(shelve.product) isEqualTo product
      assertThat(shelve.productQuantity) isEqualTo 5
    }

    it("should decrement quantity on release product") {
      val shelve = new Shelve(1, cocaCola(), 5)
      shelve.releaseProductFromShelve()

      assertThat(shelve.productQuantity) isEqualTo 4
    }

    it("should decrement quantity on release product only when quantity is positive") {
      val shelve = new Shelve(1, cocaCola(), 0)
      shelve.releaseProductFromShelve()

      assertThat(shelve.productQuantity) isEqualTo 0
    }

    it("should return TRUE when products quantity is 0 on hasNoProductsLeft method") {
      val shelve = new Shelve(1, cocaCola())

      assertThat(shelve.hasNoProductsLeft()) isTrue
    }

    it("should return FALSE when products quantity is more than 0 on hasNoProductsLeft method") {
      val shelve = new Shelve(1, cocaCola(), 1)

      assertThat(shelve.hasNoProductsLeft()) isFalse
    }
  }

}
