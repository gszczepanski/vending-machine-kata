package szczepanski.gerard.vendingmacinekata.domain.vendingmachine

/**
  * Value object which represents VendingMachine insert coni action.
  *
  * Contains product - when product is being bought
  * Contains money which may be rest (when product is returned,
  * or all money inserted to machine - when product is not present.
  *
  */
case class InsertCoinResult(val product: Option[Product] = Option.empty, val money: Option[InsertedMoney] = Option.empty) {

}
