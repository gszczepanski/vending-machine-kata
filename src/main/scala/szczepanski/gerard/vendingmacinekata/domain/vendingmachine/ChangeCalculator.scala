package szczepanski.gerard.vendingmacinekata.domain.vendingmachine {

  object ChangeCalculator {

    private val DEAD_END: Denomination = null

    private val denominationTransitionDiagram = Map[Denomination, Denomination](
      FiveZl() -> TwoZl(),
      TwoZl() -> OneZl(),
      OneZl() -> FiftyGr(),
      FiftyGr() -> TwentyGr(),
      TwentyGr() -> TenGr(),
      TenGr() -> DEAD_END
    )

    def fetchChangeFor(value: BigDecimal, moneyBag: MoneyBag): FetchChangeResult = {
      val startDenomination = FiveZl()
      collectChangeStartingFromDenomination(startDenomination, value, moneyBag)
    }

    private def collectChangeStartingFromDenomination(denomination: Denomination, value: BigDecimal, moneyBag: MoneyBag): FetchChangeResult = {
      if (isDenominationDeadEnd(denomination))
        FetchChangeResult(false, Map())

      else if (hasNoCoinsOf(denomination, moneyBag))
        collectChangeStartingFromDenomination(nextDenomination(denomination), value, moneyBag)

      else
        collectChange(denomination, value, moneyBag)
    }

    private def isDenominationDeadEnd(denomination: Denomination): Boolean = denomination == DEAD_END

    private def hasNoCoinsOf(denomination: Denomination, moneyBag: MoneyBag): Boolean = moneyBag.howMany(denomination) == 0

    private def collectChange(denomination: Denomination, value: BigDecimal, moneyBag: MoneyBag): FetchChangeResult = {
      val result = traverseForChange(
        TraverseData(denomination, value, Map(), moneyBag)
      )

      if (result.canReturnChange)
        result
      else
        collectChangeStartingFromDenomination(nextDenomination(denomination), value, moneyBag)
    }

    private def traverseForChange(data: TraverseData): FetchChangeResult = {
      if (isDenominationDeadEnd(data.currentDenomination))
        FetchChangeResult(false, Map())

      else if (noSenseToCheckCurrentDenomination(data))
        traverseForChange(
          data.copy(currentDenomination = nextDenomination(data.currentDenomination))
        )

      else
        addMoneyOfCurrentDenomination(data)
    }

    private def nextDenomination(currentDenomination: Denomination) : Denomination = denominationTransitionDiagram(currentDenomination)

    private def noSenseToCheckCurrentDenomination(data: TraverseData) : Boolean =
      hasNoCoinsOf(data.currentDenomination, data.moneyBag) || data.valueLeft < data.currentDenomination.value()

    private def addMoneyOfCurrentDenomination(data: TraverseData) : FetchChangeResult = {
      var coinsCollectedForDenomination = 0
      var moneyLeft = data.valueLeft
      var coinsForDenominationInBag = data.moneyBag.howMany(data.currentDenomination)
      val denominationValue = data.currentDenomination.value()

      while (coinsForDenominationInBag > 0 && moneyLeft >= denominationValue) {
        coinsCollectedForDenomination = coinsCollectedForDenomination + 1
        coinsForDenominationInBag = coinsForDenominationInBag - 1
        moneyLeft = moneyLeft - denominationValue
      }

      val updatedRequiredCoins = data.requiredCoins + (data.currentDenomination -> coinsCollectedForDenomination)

      if (moneyLeft.equals(BigDecimal(0.00)))
        FetchChangeResult(true, updatedRequiredCoins)

      else
        traverseForChange(
          data.copy(
            currentDenomination = nextDenomination(data.currentDenomination),
            valueLeft = moneyLeft,
            requiredCoins = updatedRequiredCoins
          )
        )
    }

    case class TraverseData(
                             val currentDenomination: Denomination,
                             val valueLeft: BigDecimal,
                             val requiredCoins: Map[Denomination, Int],
                             val moneyBag: MoneyBag
                           ) {}

  }

}


