package szczepanski.gerard.vendingmacinekata.domain.vendingmachine

/**
  * Price represents vending machine price.
  * Price not support multicurrency, thus we do not need currency distinction here.
  * Price is in polish Zloty.
  * @param value
  */
case class Price(val value : BigDecimal) {

  override def toString() : String = value.toString() + " PLN"

}
