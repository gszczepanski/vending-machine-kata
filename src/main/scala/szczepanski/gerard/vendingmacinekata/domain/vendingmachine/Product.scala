package szczepanski.gerard.vendingmacinekata.domain.vendingmachine

case class Product(val name: String, val price: Price) {

  val id: Id = Id.unique()

}
