package szczepanski.gerard.vendingmacinekata.domain.vendingmachine {

  import com.google.common.base.Preconditions.{checkArgument, checkState}

  /**
    * Represents bag of money and exposes some operations on money bag.
    * MoneyBag contains information about each Denomination quantity (how much given coins we have).
    */
  class MoneyBag {

    private var money : Map[Denomination, Int] = emptyMoney()

    private def emptyMoney(): Map[Denomination, Int] = Map[Denomination, Int](
      FiveZl() -> 0,
      TwoZl() -> 0,
      OneZl() -> 0,
      FiftyGr() -> 0,
      TwentyGr() -> 0,
      TenGr() -> 0
    )

    def withdraw(): Map[Denomination, Int] = Map[Denomination, Int] {
      val moneyToReturn = money
      money = emptyMoney()

      return moneyToReturn
    }

    def add(denomination: Denomination): Unit = {
      val increasedQuantity = money(denomination) + 1
      money = money + (denomination -> increasedQuantity)
    }

    def howMany(denomination: Denomination): Int = money(denomination)

    def subtract(denomination: Denomination, quantity: Int): Unit = {
      checkState(howMany(denomination) > 0)
      checkArgument(howMany(denomination) >= quantity)

      val subtractedQuantity = money(denomination) - quantity
      money = money + (denomination -> subtractedQuantity)
    }

    def total(): BigDecimal = money
        .flatMap(e => List(e._1.value() * e._2))
        .foldLeft(BigDecimal(0))(_ + _)

    def addMany(denominations: Map[Denomination, Int]) : Unit =
      denominations.foreach(x =>  {
        val denominationQty = money(x._1) + x._2
        money = money + (x._1 -> denominationQty)
      })
  }


}

