package szczepanski.gerard.vendingmacinekata.domain.vendingmachine

import com.google.common.base.Preconditions.checkState

class VendingMachine(
                      private val shelves: Map[Int, Shelve],
                      private val display: Display,
                      private val treasury: Treasury
                    ) {

  private var selectedShelve: Option[Shelve] = Option.empty

  def selectProduct(productNumber: Int): Unit = selectShelve(shelves(productNumber))

  def insertCoin(denomination: Denomination): InsertCoinResult = {
    checkState(selectedShelve nonEmpty)
    treasury.put(denomination)

    val product = selectedShelve.get.product
    if (treasury.isInsertedMoneyEnoughFor(product))
      return generateInsertCoinResult()
    else
      display.displayRemainingValueForProduct(treasury.howMuchMoneyIsStillRequiredFor(product))

    return InsertCoinResult()
  }

  private def selectShelve(shelve: Shelve): Unit = {
    selectedShelve = Option(shelve)
    display.displayProductPrice(shelve.product)
  }

  private def generateInsertCoinResult(): InsertCoinResult = {
    val product = selectedShelve.get.product

    if (treasury.isChangeRequiredFor(product))
      productWithChange(product)
    else
      productWithoutChange(product)
  }

  private def productWithChange(product: Product): InsertCoinResult = {
    if (treasury.canReturnChangeFor(product))
      return null
    else {
      display.displayWarningThatChangeCanNotBeReturned()
      return InsertCoinResult(
        money = Option(treasury.revertTemporaryMoney())
      )
    }
  }

  private def productWithoutChange(product: Product): InsertCoinResult = {
    treasury.transferTemporaryMoneyToMachineMoney()
    selectedShelve.get.releaseProductFromShelve()

    return InsertCoinResult(product = Option(product))
  }

  def cancel(): InsertedMoney = {
    selectedShelve = Option.empty
    treasury.revertTemporaryMoney()
  }

}
