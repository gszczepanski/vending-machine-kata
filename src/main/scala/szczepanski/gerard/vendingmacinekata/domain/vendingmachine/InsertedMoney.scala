package szczepanski.gerard.vendingmacinekata.domain.vendingmachine

/**
  * Represents money inserted by customer.
  * Actually this is value object wrapper for map of denominations and its quantities,
  * to make method signature more meaningful.
  */
case class InsertedMoney(val denominationQuantityMap: Map[Denomination, Int]) { }
