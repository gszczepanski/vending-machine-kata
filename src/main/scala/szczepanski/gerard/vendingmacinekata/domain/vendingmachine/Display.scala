package szczepanski.gerard.vendingmacinekata.domain.vendingmachine {


  class Display {

    def displayProductPrice(product: Product): Unit = println(product.price)

    def displayRemainingValueForProduct(value: BigDecimal): Unit = println("Remaining value for product: " + value + " PLN")

    def displayWarningThatChangeCanNotBeReturned(): Unit = println("Change can not be returned. Money inserted for product is being returned to you")

  }

}


