package szczepanski.gerard.vendingmacinekata.domain.vendingmachine {

  class Treasury {

    private val temporaryMoney = new MoneyBag()
    private val machineMoney = new MoneyBag()

    def put(denomination: Denomination): Unit = temporaryMoney.add(denomination)

    def isInsertedMoneyEnoughFor(product: Product): Boolean = howMuchMoneyIsStillRequiredFor(product) <= 0

    def howMuchMoneyIsStillRequiredFor(product: Product): BigDecimal = product.price.value - temporaryMoney.total()

    def revertTemporaryMoney(): InsertedMoney = InsertedMoney(temporaryMoney.withdraw())

    def isChangeRequiredFor(product: Product): Boolean = temporaryMoney.total() > product.price.value

    def transferTemporaryMoneyToMachineMoney() : Unit = machineMoney.addMany(temporaryMoney.withdraw())

    def canReturnChangeFor(product: Product) : Boolean = {
      val requiredChange = temporaryMoney.total() - product.price.value
      ChangeCalculator.fetchChangeFor(requiredChange, machineMoney).canReturnChange
    }



  }

}

