package szczepanski.gerard.vendingmacinekata.domain.vendingmachine

import java.util.UUID


case class Id private (val value: String) { }

object Id {
  def unique(): Id = Id(UUID.randomUUID.toString.replaceAll("-", ""))
}
