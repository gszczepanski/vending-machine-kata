package szczepanski.gerard.vendingmacinekata.domain.vendingmachine

sealed trait Denomination {
  def value(): BigDecimal
}

case class FiveZl() extends Denomination {
  override def value(): BigDecimal = 5.0
}

case class TwoZl() extends Denomination {
  override def value(): BigDecimal = 2.0
}

case class OneZl() extends Denomination {
  override def value(): BigDecimal = 1.0
}

case class FiftyGr() extends Denomination {
  override def value(): BigDecimal = 0.5
}

case class TwentyGr() extends Denomination {
  override def value(): BigDecimal = 0.2
}

case class TenGr() extends Denomination {
  override def value(): BigDecimal = 0.1
}
