package szczepanski.gerard.vendingmacinekata.domain.vendingmachine

case class FetchChangeResult(val canReturnChange: Boolean, val requiredDenominations: Map[Denomination, Int]) { }
