package szczepanski.gerard.vendingmacinekata.domain.vendingmachine

class Shelve(val number: Int, val product: Product, private var quantity: Int = 0) {

  def productQuantity = quantity

  def releaseProductFromShelve(): Unit = if (quantity > 0) quantity = quantity - 1

  def hasNoProductsLeft(): Boolean = quantity == 0

}
